package com.samples.appbotomnav.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.samples.appbotomnav.R;
import com.samples.appbotomnav.ui.ui.main.NewFragment;

public class New extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, NewFragment.newInstance())
                    .commitNow();
        }
    }
}